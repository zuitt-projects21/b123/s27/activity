let http = require("http");

let users = [

	{
		name: "jeff_chan17",
		password: "ballislife",
	},
	{
		name: "d_miranda0",
		password: "mirandaPG",
	},
	{
		name: "arwind_spiderMan",
		password: "imspiderMan",
	}

];


http.createServer(function(req,res){


	if(req.url === "/users" && req.method === "GET"){

		res.writeHead(200,{'Content-Type':'application/json'});
		res.end(JSON.stringify(users));

	} else if(req.url === "/users" && req.method === "POST"){

		let requestBody = "";

		req.on('data', function(data){

			console.log(data);
			requestBody += data;

		})

		req.on('end', function(){

			console.log(requestBody);
			requestBody = JSON.parse(requestBody);

			let newUser = {

				"name": requestBody.name,
				"password": requestBody.password

			}

			users.push(newUser);
			console.log(users);

			res.writeHead(200, {'Content-Type': "application/json"});
			res.end(`Registration Successful.`);
		})
	

	} else if(req.url === "/users/login" && req.method === "POST"){

		let requestBody = "";

		req.on('data', function(data){

			console.log(data);
			requestBody += data;

		})

		req.on('end', function(){

			console.log(requestBody);
			requestBody = JSON.parse(requestBody);

			let loginUser = {

				"name": requestBody.name,
				"password": requestBody.password

			}

			function login(username, password){

				let userFound = users.find((user)=>{
					return user.name === username && user.password === password
				});

				console.log(userFound);

				if (userFound){
					res.writeHead(200, {'Content-Type': "application/json"});
					res.end(`Login Successful.`);
				}else{
					res.writeHead(404, {'Content-Type': "application/json"});
					res.end(`Login Failed. Wrong Credentials.`);	
				}

			}

			login(loginUser.name, loginUser.password);

		})

		
	} else{
		res.writeHead(404,{'Content-Type ':'text/plain'});
		res.end('Resouce Not Found!')
	}



}).listen(8000);